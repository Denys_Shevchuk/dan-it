'use strict';
const burger = document.querySelector(".header-menu__hamburger");
const menu = document.querySelector(".navigation-block__menu");
burger.addEventListener("click", () => {
    menu.classList.toggle("navigation-block__menu-active");
    burger.classList.toggle("header-menu__hamburger-active");
});


